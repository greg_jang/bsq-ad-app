import React, { Component } from "react";
import { ScrollView } from "react-native";
import Ad from './VideoAd';
import Ad28 from "../images/ads/28.mp4";
import Ad29 from "../images/ads/29.mp4";
import { Viewport } from '@skele/components';

export default class VideoScrollView extends Component {
  render() {
    return (
      <Viewport.Tracker>
        <ScrollView
          horizontal={true}
          pagingEnabled={true}
          scrollEventThrottle={16}
        >
            <Ad source={Ad28} />
            <Ad source={Ad29} />
        </ScrollView>
      </Viewport.Tracker>
    );
  }
}



// export default VideoScrollView;
