import React, { Component } from 'react';
import { View, Dimensions, StyleSheet } from "react-native";
import Video from "react-native-video";
import { Viewport } from "@skele/components";

const ViewportAwareVideo = Viewport.Aware(Video);

export class VideoAd extends Component {

  render() {
    this.state = {
      paused: true,
      visible: false
    };

    return (
      <View style={styles.viewContainer}>
        <ViewportAwareVideo
          source={this.props.source}
          paused={this.state.paused}
          style={{ width: "100%", height: "100%" }}
          innerRef={ref => (this.player = ref)}
          onViewportEnter={() => console.log("entered")}
          onViewportLeave={() => console.log("left")}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    width: Dimensions.get("window").width,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default VideoAd
