import React, { Component } from 'react';
import { View, Image, StyleSheet } from "react-native";
// import MenuIcon from '../images/ic_menu.svg';

export class Menu extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.menu} source={require("../images/ic_menu.png")} />
        <Image style={styles.search} source={require("../images/ic_search.png")} />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    position: "absolute",
    width: "100%",
    height: "100%",
    // top: 0,
    // left: 0,
    padding: 50,
    backgroundColor: "red"
  },
  menu: {
    position: "absolute",
    top: 0,
    right: 0
  },
  search: {
    position: "absolute",
    top: 0,
    left: 0
  }
});

export default Menu
